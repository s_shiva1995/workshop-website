﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="cloud-workshop.aspx.cs" Inherits="cloud_workshop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
    <script type="text/javascript" src="JavaScript.js"></script>
    <link type="text/css" rel="stylesheet" href="StyleSheet.css" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
    <div>
        <div class="menu">
            MENU
        </div>
        <div class="head">
            <img src="image/ims11.jpg" style="width: 19.5%; height: 150px; border-radius: 13px"/>
            <!--<img src="image/cloud+big_data.jpg" style="width: 80%; height: 150px"/>-->
            <!--<div style="width: 80%; height: 150px; display: inline">-->
                <img src="image/peek.jpg" style="width: 80%; height: 150px"/>
            <!--</div>-->
        </div>
        <div class="nav">
            <div class="close"></div>
            <ul class="element">
                <li class="home">
                    home
                </li>
                <li class="aboutwork">
                    about workshop
                </li>
                <li class="objective">
                    objective
                </li>
                <li class="schedule">
                    schedule
                </li>
                <li>
                    <asp:HyperLink ID="hpl_register" runat="server" Text="register" NavigateUrl="https://docs.google.com/forms/d/1D4BxPwXLAOWB50QYQfu_NTarqoFpk08beFhe1ogv39M/viewform" CssClass="link"></asp:HyperLink>
                </li>
                <li class="about">
                    about
                </li>
                <li class="brochure">
                    brochure
                </li>
                <li class="contact">
                    contact us
                </li>
                <li class="feedback">
                    give feedback
                </li>
                <li class="sworkshop">
                    workshops
                </li>
            </ul>
        </div>
        <div class="image">
            <img src="image/_MG_7470.JPG" class="l"/>
            <img src="image/DSC_7838.JPG" class="l"/>
            <img src="image/DSC02780.JPG" class="l"/>
            <img src="image/DSC02781.JPG" class="l"/>
            <img src="image/ims1.JPG" class="l"/>
        </div>
        <div class="welcome">
            <p>Welcome to Faculty Development Program</p>
        </div>
        <div class="detail" style="height: 400px">
            <div class="workshop">
                <p>About the Workshop</p>
            </div>
            <div class="wdetail">
                <p>Cloud computing is a computing term or metaphor that evolved in the late 2000s, based on utility and consumption of computer resources. Cloud computing involves deploying groups of remote servers and software networks that allow different kinds of data sources be uploaded for real time processing to generate computing results without the need to store processed data on the cloud. Clouds can be classified as public, private or hybrid.</p>
                <p>Big data is a broad term for data sets so large or complex that traditional data processing applications are inadequate. Challenges include analysis, capture, curation, search, sharing, storage, transfer, visualization, and information privacy. The term often refers simply to the use of predictive analytics or other certain advanced methods to extract value from data, and seldom to a particular size of data set.</p>
                <p><b>Venue:</b> IMS Engineering College</p>
                <p>NH-24, Adhyatmik Nagar</p>
                <p>Near Dasna</p>
                <p>Ghaziabad. <b>Pin Code:</b> 201009</p>
            </div>
        </div>
        <div class="detail" style="height: 180px">
            <div class="workshop">
                <p>objective</p>
            </div>
            <div class="wdetail">
                <p>The aim of the proposed FDP is to provide a common learning platform for the researchers and faculty members working in the areas of cloud computing and Big Data. The FDP is expected to encourage & motivate the participants to take up research and contribute in the areas of cloud computing and big data. This should be a useful platform for participants to learn about recent research areas and problems in the field of cloud computing and big data.</p>
            </div>
        </div>
        <div class="detail">
            <div class="workshop">
                <p>schedule</p>
            </div>
            <div class="wdetail">
                <p>Coming Soon</p>
            </div>
        </div>
        <div class="detail" style="height: 800px">
            <div class="workshop">
                <p>about imsec</p>
            </div>
            <div class="wdetail">
                <p>IMS Engineering College, Ghaziabad is one of the top notch engineering colleges of the entire Delhi NCR by virtue of its ISO 9001:2008 certification by United Kingdom Accreditation Service and American National Accreditation Board (UKAS & ANAB). It is a NAAC Accredited Institution for maintaining world class quality in education & infrastructure. The highly qualified and committed faculty, state-of-the-art laboratories, Computer Centre and Learning Resource Centre, with its conducive environment, provide the students the most exciting and gainful opportunities for acquiring knowledge and technical expertise, to groom and orient the young minds. The college is accredited with “Tata Consultancy Services” for placements and training projects. The institution has made excellent progress in placing majority of the students in reputed national and international companies with multiple offers.</p>
                <p>A professional career in engineering and technology provides enormous opportunities to realize your dreams. High-quality education along with
                   holistic development of personality is an essential prerequisite for a top notch career.</p>
                <p>IMS Engineering College is a premier technological institution conceptualized and designed to impart education for knowledge acquisition and professionalism. Teaching learning processes and practices nurture creativity and innovation with the objective of enhancing proficiency and productivity. With unflagging zeal, IMSEC continues to serve as a yardstick of quality for fostering improvement in the field of technical and management education. Its clear focus on creativity and innovation has wakened students from the realm of unknown.</p>
                <h1>MOTTO</h1>
                <p>IMSEC nurtures, nourishes and grooms future engineers and managers with professional excellence and humane outlook with the envisioned motto:
                   I : Innovation
                   M : Management
                   S : Sublimity
                   E : Engineering
                   C : Creativity</p>
                <h1>VISION</h1>
                <p>• To develop IMSEC as a centre of excellence in technical and management education.</p>
                <p>• To inculcate the qualities of leadership, professionalism, corporate
                    understanding and executive competence.</p>
                <p>• To imbibe and enhance human values, ethics and morals in our students.</p>
                <p>• To transform students into globally competitive professionals.</p>
                <h1>MISSION</h1>
                <p>“Our mission is to impart vibrant innovative and global education and to make IMS the world leader in terms of excellence of education, research and to serve the nation in the 21st century.”</p>
            </div>
        </div>
        <div class="detail">
            <div class="workshop">
                <p>brochure</p>
            </div>
            <div class="wdetail">
                <p>Coming Soon</p>
            </div>
        </div>
        <div class="detail" style="height: 820px">
            <div class="workshop">
                <p>contact us</p>
            </div>
            <div class="wdetail">
                <h1 style="color: black">CONVENER</h1>
                <h2>Mr. Pankaj Agarwal</h2>
                <p><i>HOD (CS)</i></p>
                <p><b>Contact No:</b> 09999940157</p>
                <p><b>E-mail id:</b> pankaj7877@gmail.com </p>
                <!--<h1 style="color: black">EVENT ORGANISER</h1>-->
                <h2>Mr. Mayank Chandra</h2>
                <p><i>Assistant Professor</i></p>
                <p><b>Contact No:</b> 09268193071,(91)120-3013800</p>
                <p><b>E-mail id:</b> machandra100@gmail.com</p>
                <p></p>
                <h2>Mr. Naveen Kumar</h2>
                <p><i>Assistant Professor</i></p>
                <p><b>Contact No:</b> 08826638778</p>
                <p><b>E-mail id:</b> naveencurca@gmail.com</p>
                <h1 style="color: black">STUDENT COORDINATOR</h1>
                <h4>SHIVANSH SINGH(09711582696)</h4>
                <h4>MUKTA TIWARI</h4>
                <h4>ASHUTOSH(09911015078)</h4>
                <h4>AAKASH PATEL(08802564963)</h4>
                <h4>SOHA KHAN</h4>
            </div>
        </div>
        <div class="detail" style="height: 200px">
            <div class="workshop">
                <p>workshops</p>
            </div>
            <div class="wdetail"> 
                <p><a href="http://fdpimsec.cse-imsec.com/">Faculty Development Program: Machine Learning</a></p>
                <p><a href="http://netcampimsec.blogspot.in/">Faculty Development Program: Ethical Hacking</a></p>
            </div>
        </div>
        <div class="foot">
            <p>Copyright © 2015 Cloud Computing and Big Data Developed By: Shivansh Singh</p>
        </div>
    </div>
</body>
</html>
