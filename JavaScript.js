﻿var main = function () {
    $('.menu').click(function () {
        $('.nav').animate({ left: '-3%' }, 200);
    });
    $(".close").click(function () {
        $('.nav').animate({ left: '-20%' }, 200);
    });
    $('.home').click(function () {
        $("html, body").animate({ scrollTop: '0' }, 300);
    });
    $('.aboutwork').click(function () {
        $("html, body").animate({ scrollTop: '620px' }, 300);
    });
    $('.objective').click(function () {
        $("html, body").animate({ scrollTop: '1040px' }, 300);
    });
    $('.schedule').click(function () {
        $("html, body").animate({ scrollTop: '1235px' }, 300);
    });
    $('.about').click(function () {
        $("html, body").animate({ scrollTop: '1770px' }, 300);
    });
    $('.brochure').click(function () {
        $("html, body").animate({ scrollTop: '2585px' }, 300);
    });
    $('.contact').click(function () {
        $("html, body").animate({ scrollTop: '3120px' }, 300);
    });
    $('.sworkshop').click(function () {
        $('html, body').animate({ scrollTop: '3600px' }, 200);
    });
}
$(document).ready(main);
$(window).load(function () {
    init = function () {
        var initialFadeIn = 1000;
        var itemInterval = 5000;
        var fadeTime = 2500;
        var numberOfItems = $('.l').length;
        var currentItem = 0;
        $('.l').eq(currentItem).fadeIn(initialFadeIn);
        infiniteLoop = setInterval(function () {
            $('.l').eq(currentItem).fadeOut(fadeTime);
            if (currentItem == numberOfItems - 1) {
                currentItem = 0;
            } else {
                currentItem++;
            }
            $('.l').eq(currentItem).fadeIn(fadeTime)
        }, itemInterval);
    }
    init();
});