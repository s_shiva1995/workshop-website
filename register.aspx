﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="stylesheet" href="StyleSheet.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="menu">
        <a href="cloud-workshop.aspx">GO BACK TO HOME</a>
    </div>
    <div class="head">
            <img src="image/ims11.jpg" style="width: 19.5%; height: 150px; border-radius: 13px"/>
            <div style="width: 80%; height: 150px; display: inline">
                <img src="image/peek.jpg" style="width: 80%; height: 150px"/>
            </div>
    </div>
        <asp:ScriptManager ID="scr1" runat="server"></asp:ScriptManager>
    <div class="register">
        <h1 class="pop">REGISTER</h1>
        <asp:TextBox ID="txtname" runat="server" CssClass="txtbox"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtname" WatermarkText="Enter Your Name"></asp:TextBoxWatermarkExtender>
        <asp:FilteredTextBoxExtender ID="p" runat="server" TargetControlID="txtname" ValidChars="qwertyuioplkjhgfdsazxcvbnm ASDFGHJKLMNBVCXZQWERTYUIOP"></asp:FilteredTextBoxExtender>
        <asp:RequiredFieldValidator ID="lop" runat="server" ControlToValidate="txtname" ErrorMessage="*required field"></asp:RequiredFieldValidator>
        <asp:DropDownList ID="droptype" runat="server" CssClass="txtbox">
            <asp:ListItem>Industry</asp:ListItem>
            <asp:ListItem>Research Scholar/Faculty</asp:ListItem>
            <asp:ListItem>Student</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtaffiliation" runat="server" CssClass="txtbox"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="txt" runat="server" TargetControlID="txtaffiliation" WatermarkText="Enter your Affiliation"></asp:TextBoxWatermarkExtender>
        <asp:RequiredFieldValidator ID="pop" runat="server" ControlToValidate="txtaffiliation" ErrorMessage="*required field"></asp:RequiredFieldValidator>
        <asp:TextBox ID="txtemail" runat="server" CssClass="txtbox"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="tx" runat="server" TargetControlID="txtemail" WatermarkText="Enter your Email-ID"></asp:TextBoxWatermarkExtender>
        <asp:RegularExpressionValidator ID="l" runat="server" ControlToValidate="txtemail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Enter Valid Email"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="poi" runat="server" ControlToValidate="txtemail" ErrorMessage="*required field"></asp:RequiredFieldValidator>
        <asp:TextBox ID="txtmobile" runat="server" CssClass="txtbox"></asp:TextBox>
        <asp:RequiredFieldValidator ID="fop" runat="server" ControlToValidate="txtmobile" ErrorMessage="*required field"></asp:RequiredFieldValidator>
        <asp:TextBoxWatermarkExtender ID="t" runat="server" TargetControlID="txtmobile" WatermarkText="Enter your Mobile no"></asp:TextBoxWatermarkExtender>
        <asp:FilteredTextBoxExtender ID="i" runat="server" TargetControlID="txtmobile" ValidChars="0123456789"></asp:FilteredTextBoxExtender>
        <br />
        <asp:Button ID="btnsubmit" runat="server" Text="Register" CssClass="btn" OnClick="btnsubmit_Click"/>
    </div>
    </form>
</body>
</html>
